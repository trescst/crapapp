import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const PlayingFieldRowPlayer = (props) => {
    return (
        <Fragment>
            <div className="col s2">{props.index}</div>
            {typeof props.ship === 'undefined' ?
                <Fragment>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                    <div className="playingField col s1"></div>
                </Fragment>
                :
                <Fragment>
                    {props.ship.includes('A') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('B') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('C') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('D') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('E') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('F') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('G') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('H') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('I') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                    {props.ship.includes('J') ?
                        <div className="playingField ship col s1"></div>
                        : <div className="playingField col s1"></div>
                    }
                </Fragment>
            }
        </Fragment>
    );
}

PlayingFieldRowPlayer.propTypes = {
    index: PropTypes.number,
    ship: PropTypes.array
}

export default PlayingFieldRowPlayer;