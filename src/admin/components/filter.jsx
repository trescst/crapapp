import React, { Fragment } from "react";
import PropTypes from "prop-types";


const Filter = (props) => {
    return (
        <Fragment>
            <div className="row">
                <div className="col s5">
                    <div className="input-field row">
                        <input id="username" type="text" onChange={props.updateUsernameToSearch}/>
                        <label htmlFor="username">USERNAME</label>
                    </div>
                </div>
                <div className="col s2 offset-s1">
                    <a id="filter" className="btn btn-large waves-effect waves-light logout" onClick={props.filter}><i className="material-icons">search</i></a>
                </div>
                <div className="col s4">
                    <a id="remove_filter" className="btn btn-large waves-effect waves-light redButton" onClick={props.removeFilters}>Remove filters</a>
                </div>
            </div>
        </Fragment>
    );
}

Filter.propTypes = {
    updateUsernameToSearch: PropTypes.func,
    filter: PropTypes.func,
    removeFilters: PropTypes.func
}

export default Filter;