import React, { Component } from 'react';
import { getBugsFromDB } from '../../shared/services/bugs';
import BugsView from './bugsView';
import LoadingScreen from '../../shared/components/loadingScreen';


class RedirectToBug extends Component {
    constructor(props) {
        super(props);

        this.state ={
            bugs: []
        }

        getBugsFromDB().then((r) => {
            if (r.status === 200) {
                r.text().then((bugs) => {
                    setTimeout(() => {
                        this.setState({bugs: bugs});
                }, 1);
                    window.sessionStorage.setItem('bugs', bugs);
                });
            }
        });
    }

    render() {
            return (
                this.state.bugs.length === 0?
                <LoadingScreen />
                : <BugsView />
            )
    }
}

export default RedirectToBug;